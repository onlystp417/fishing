let gameId, player;

function postData() {
  console.log('postData');
  $.ajax({
    type: "POST",
    url: "https://655726a1.ngrok.io/api/games",
    data: JSON.stringify({
      'name': $('#userName').val()
    }),
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (response) {
      gameId = response.game_id;
      player = $('#userName').val();
      console.log(response);
      location.href = `./room.html?player=${player}&gameId=${gameId}`;
    },
    failure: function (errMsg) {
      console.log(errMsg);
    }
  });
}

$('.goRoom').click(function () {
  postData();

})