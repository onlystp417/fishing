let urlParams = new URLSearchParams(window.location.search);
let gameId = urlParams.get('gameId')

postData();

function postData() {
  console.log('postData');
  $.ajax({
    type: "POST",
    url: "https://655726a1.ngrok.io/api/results",
    data: JSON.stringify({
      game_id: gameId
    }),
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (data) {
      console.log(data);
      let results = [];
      results = data.results;
      console.log(results);
      updateList();
    },
  });
}

function updateList() {
  $('results').each(function (i, item) {
    console.log(item);
    var htmlItem = `
    <tr>
      <td>${i + 1}</td>
      <td>${item.name}</td>
      <td>${item.score}</td>
    </tr>
    `;
    $('.result').append(htmlItem);
  })
}

$('.back').click(function () {
  location.href = './index.html';
})