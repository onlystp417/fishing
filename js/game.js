let screenWidth = $(window).width();

let screenHeight = $(window).height();

let getStart = screenWidth / 2 - 200;

let getEnd = screenWidth / 2 + 200;

let xLine = 0;

let yLine = 0;

let urlParams = new URLSearchParams(window.location.search);
let playName = urlParams.get('player');
let gameId = urlParams.get('gameId')

function getRandomInt(max) {
  return (Math.floor(Math.random() * Math.floor(max))) + 1;
}
line();

function line() {
  window.setInterval(function () {
    xLine = getRandomInt(screenWidth);
    yLine = getRandomInt(screenHeight);
    $('.move').attr('style', `transform: translate(${xLine}px, ${yLine}px); transition: 1s`);
  }, 1000);
}

let score = 0;

function getFish() {
  let xLine = getRandomInt(screenWidth);
  if (xLine > getEnd) {
  } else if (xLine < getStart) {
  } else {
    score = score + 1;
  }
  $('.score').text(`<目前釣到${score}分`);
  return score;
}

timesout();

function timesout() {
  window.setTimeout(function () {
    postData(event);
    alert('Game Over');
    // location.href = './board.html';
  }
    , 15000);

}

function postData(event) {
  console.log('postData');
  $.ajax({
    type: "POST",
    url: "https://655726a1.ngrok.io/api/scores",
    data: JSON.stringify({
      score: getFish(),
      name: playName,
      game_id: gameId
    }),
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (data) {
      console.log(data);
      location.href = `./board.html?gameId=${gameId}`;
    },
    failure: function (errMsg) {
      alert(errMsg);
    }
  });
}
